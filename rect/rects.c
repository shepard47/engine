#include <rect.h>

#include <stdlib.h>

rect*
rects(int cnt)
{
	rect* arr = malloc(sizeof(rect) * cnt);

	int indices[cnt][6];
	for (int i = 0; i < cnt; ++i) {
		indices[i][0] = 0 + (i * 4);
		indices[i][1] = 1 + (i * 4);
		indices[i][2] = 3 + (i * 4);
		indices[i][3] = 1 + (i * 4);
		indices[i][4] = 2 + (i * 4);
		indices[i][5] = 3 + (i * 4);
	}

	int vb;
	int va = newvad(&vb, sizeof(float) * 12 * cnt, indices, sizeof(indices));
	
	return arr;
}