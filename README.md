# Engine

This game engine does not have a name yet.

## Usage


### Requirements for building

To be able to build the engine you need to have: a c compiler and  the mk build system.

### Installing

First, you should change the mklib file as you want.

You can customize there what compiler you would like to use:

```
cc=clang
flags=-c
```

After that, you can build the libraries using mk:

```
mk
```

### Linking

To use the engine in your program, just link the libraries you want to use.
These libraries are in the bin folder.

```
cc -o mygame main.c -lws -ldraw ...
```

## License

This project is licensed under the BSD License - see the [LICENSE](LICENSE) file for details


