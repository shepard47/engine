#include <win.h>

#include <X11/Xlib.h>
#include <GL/glx.h>
#include <stdio.h>
#include <stdlib.h>


void
swapbuf(win* win)
{
	glXSwapBuffers(win->dis, win->winid);
	return;
}