#include <win.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <GL/glx.h>
#include <stdio.h>
#include <stdlib.h>

#define GLX_CONTEXT_MAJOR_VERSION_ARB 0x2091
#define GLX_CONTEXT_MINOR_VERSION_ARB 0x2092
typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);

// TODO: omit these globals
XVisualInfo *vi;

void
getvis(Display* dis, int* best)
{
	GLXFBConfig *fbc;

	int attr[] = {
		GLX_X_RENDERABLE    , True,
		GLX_DRAWABLE_TYPE   , GLX_WINDOW_BIT,
		GLX_RENDER_TYPE     , GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE   , GLX_TRUE_COLOR,
		GLX_RED_SIZE        , 8,
		GLX_GREEN_SIZE      , 8,
		GLX_BLUE_SIZE       , 8,
		GLX_ALPHA_SIZE      , 8,
		GLX_DEPTH_SIZE      , 24,
		GLX_STENCIL_SIZE    , 8,
		GLX_DOUBLEBUFFER    , True,
		None
	};

	int maj, min;
	if (!glXQueryVersion(dis, &maj, &min) || ((maj == 1) && (min < 3)) || (maj < 1)) {
		puts("GLX version is not correspondent.");
		exit(-1);
	}

	int fbcnt;
	fbc = glXChooseFBConfig(dis, DefaultScreen(dis), attr, &fbcnt);
	if (!fbc){
		puts("Failed to retrieve a framebuffer config"); 
		exit(-1);
	}

	int best_fbc = -1, worst_fbc = -1, best_num_samp = -1, worst_num_samp = 999;

	for (int i = 0; i < fbcnt; ++i) {
		XVisualInfo *tvi = glXGetVisualFromFBConfig(dis, fbc[i]);
		if (tvi) {
			int samp_buf, samples;
			glXGetFBConfigAttrib(dis, fbc[i], GLX_SAMPLE_BUFFERS, &samp_buf);
			glXGetFBConfigAttrib(dis, fbc[i], GLX_SAMPLES, &samples);

			if (best_fbc < 0 || samp_buf && samples > best_num_samp)
				best_fbc = i, best_num_samp = samples;
			if (worst_fbc < 0 || !samp_buf || samples < worst_num_samp)
				worst_fbc = i, worst_num_samp = samples;
		}
		XFree(tvi);
    }

	*best = fbc[best_fbc];
	XFree(fbc);
	vi = glXGetVisualFromFBConfig(dis, *best);

	return;
}

void
mkcmap(Display* display, XSetWindowAttributes* swa)
{
	printf( "Creating colormap\n" );
	swa->colormap = XCreateColormap(display, RootWindow(display, vi->screen), vi->visual, AllocNone);
	swa->background_pixmap = None ;
	swa->border_pixel = 0;
	swa->event_mask = ExposureMask | KeyPressMask | ButtonPress |
                          StructureNotifyMask | ButtonReleaseMask |
                          KeyReleaseMask | EnterWindowMask | LeaveWindowMask |
                          PointerMotionMask | Button1MotionMask | VisibilityChangeMask |
                          ColormapChangeMask;

	return;
}

void
mkwin(Display* display, int* window, XSetWindowAttributes* swa)
{
    printf( "Creating window\n" );
    *window = XCreateWindow(display, DefaultRootWindow(display), 0, 0, 1366, 768, 0, vi->depth, InputOutput, vi->visual, CWBorderPixel|CWColormap|CWEventMask, swa);
    if (!*window){
    	puts("Failed to create window.");
    	exit(-1);
    }
    XFree(vi);
    XStoreName( display, *window, "GL 4.6 Window" );
    printf( "Mapping window\n" );
    XMapWindow( display, *window );

    return;
}

void
mkctx(Display* display, int window, int best)
{
	glXCreateContextAttribsARBProc glXCreateContextAttribsARB = 0;
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc) glXGetProcAddressARB((const unsigned char*)"glXCreateContextAttribsARB");

	GLXContext ctx = 0;

	int context_attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
		GLX_CONTEXT_MINOR_VERSION_ARB, 6,
		None
	};

    printf("Creating context\n");
    ctx = glXCreateContextAttribsARB(display, best, 0, True, context_attribs);

    XSync( display, False );
    
    printf( "Created GL 4.6 context\n" );

    XSync(display, False);
  
    if (!glXIsDirect(display, ctx)) {
        printf( "Indirect GLX rendering context obtained\n" );
    }
    else {
        printf( "Direct GLX rendering context obtained\n" );
    }

    printf( "Making context current\n" );
    glXMakeCurrent( display, window, ctx );

    return;
}

win* 
newwin(char* name)
{
	win* wp = malloc(sizeof(win));

	int win;
	XSetWindowAttributes swa;
	int best;
    Display* dis = XOpenDisplay(0);
    if (!dis) {
    	printf("Failed to open X display\n");
    	exit(-1);
    }
    wp->dis = dis;

    getvis(dis, &best);
    mkcmap(dis, &swa);
    mkwin(dis, &win, &swa);
    mkctx(dis, win, best);

    wp->winid = win;

	return wp;
}