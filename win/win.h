typedef struct win {
	char* 	name;
	void* 	win;
	void*	dc;
	void*	dis;
	int		winid;
} win;

enum {
	opengl,
	vulkan,
	direct
};

win*	newwin(char*);
void 	swapbuf(win*); // TODO: gain more knowledge

void  	procev(void*, void*);

void (*keydowncb)(int);

