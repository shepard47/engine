#include <win.h>

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

void
swapbuf(win* win)
{
	SwapBuffers(win->dc);
	return;
}