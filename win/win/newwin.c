#include <win.h>

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

LRESULT CALLBACK winproc(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp);

void
regwc(HINSTANCE inst, char* name)
{
	WNDCLASSEX wc;

	wc.cbSize			= sizeof(WNDCLASSEX);
	wc.style			= CS_OWNDC;
	wc.lpfnWndProc		= winproc;
	wc.cbClsExtra		= 0;
	wc.cbWndExtra		= 0;
	wc.hInstance		= inst;
	wc.hIcon			= LoadIcon(0, IDI_APPLICATION);
	wc.hCursor			= LoadCursor(0, IDC_ARROW);
	wc.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszMenuName		= 0;
	wc.lpszClassName	= name;
	wc.hIconSm			= LoadIcon(0, IDI_APPLICATION);

	if (!RegisterClassEx(&wc)) {
		puts("Failed to register class.");
		exit(-1);
	}

	return;
}

void
mkwin(HINSTANCE inst, HWND* hwnd, char* name)
{
	*hwnd = CreateWindowEx(WS_EX_CLIENTEDGE, name, name, WS_OVERLAPPEDWINDOW, 0, 0, 800, 600, 0, 0, inst, 0);
	if (*hwnd == 0) {
		puts("Failed to create window.");
		exit(-1);
	}

	ShowWindow(*hwnd, SW_SHOW);
	UpdateWindow(*hwnd);

	return;
}

void
mkctx(HWND* hwnd)
{
	HDC dc = GetDC(*hwnd);

	PIXELFORMATDESCRIPTOR pfd = {
		sizeof(PIXELFORMATDESCRIPTOR), 1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA, 32,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		24, 8, 0, PFD_MAIN_PLANE, 0, 0, 0, 0
	};

	int fm = ChoosePixelFormat(dc, &pfd);
	if (fm == 0) {
		puts("Failed to choose pixel format.");
		exit(-1);
	}
	if (!SetPixelFormat(dc, fm, &pfd)) {
		puts("Failed to set pixel format.");
		exit(-1);
	}
	HGLRC old = wglCreateContext(dc);
	if (!wglMakeCurrent(dc, old)) {
		puts("Failed to make old context current.");
		exit(-1);
	}

	int attr[] = {
		0x2091, 4,
		0x2092, 6,
		0x9126, 0x0001,
		0x2094, 0x0002,
		0
	};

	HGLRC ctx = ((HGLRC(*)(HDC, HGLRC, int*))(wglGetProcAddress("wglCreateContextAttribsARB")))(dc, old, attr);
	wglMakeCurrent(dc, ctx);
	wglDeleteContext(old);

	return;
}

win* 
newwin(char* name)
{
	win* wp = malloc(sizeof(win));

	HWND hwnd;
	HINSTANCE inst = GetModuleHandle(0);

	regwc(inst, name);
	mkwin(inst, &hwnd, name);
	#ifdef opengl
	mkctx(&hwnd);
	#endif

	wp->win	= &hwnd;
	wp->dc	= GetDC(hwnd);

	return wp;
}