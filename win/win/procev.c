#include <win.h>

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

LRESULT CALLBACK 
winproc(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp)
{
	switch(msg) {
	case WM_KEYDOWN:
		keydown(wp);
	break;
	case WM_KEYUP:
		keyup(wp);
	break;
	case WM_SIZE:
		resize(LOWORD(lp), HIWORD(lp));
	break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		exit(0);
	break;
	default:
		return DefWindowProc(hwnd, msg, wp, lp);
	}
	return 0;
}

void 
procev(void* nil, void* msg)
{
	GetMessage(msg, 0, 0, 0);
	TranslateMessage(msg);
	DispatchMessage(msg);

	return;	
}