#include <drw.h>
#include <epoxy/gl.h>
#include <stdio.h>

int
newshader(char* code, int type)
{
	int shader = glCreateShader(type);
	glShaderSource(shader, 1, &code, 0);
	glCompileShader(shader);

	int success;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success) {
		char log[1024];
		glGetShaderInfoLog(shader, 1024, 0, log);
		printf("%s\n", log);
	}

	return shader;
}