#include <drw.h>
#include <epoxy/gl.h>

void
useprogram(int program)
{
	glUseProgram(program);
	return;
}