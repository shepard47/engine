#include <drw.h>
#include <epoxy/gl.h>
#include <stdio.h>
#include <stdarg.h>

int
newprogram(int vs, int gs, int fs)
{
	int program = glCreateProgram();
	glAttachShader(program, vs);
	glAttachShader(program, gs);
	glAttachShader(program, fs);
	glLinkProgram(program);

	int success;
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success) {
		char log[1024];
		glGetProgramInfoLog(program, 1024, 0, log);
		printf("%s\n", log);
	}

	return program;
}