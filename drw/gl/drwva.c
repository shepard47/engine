#include <drw.h>
#include <epoxy/gl.h>

void
drwvas(int va)
{
	glBindVertexArray(va);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	return;
}

void
drwvad(int va, int vb, float* vertices, int vsize, int* indices, int isize)
{
	glBindBuffer(GL_ARRAY_BUFFER, vb);
	glBufferSubData(GL_ARRAY_BUFFER, 0, vsize, vertices);

	glBindVertexArray(va);
	glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, 0);

	return;
}