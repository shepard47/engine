void initdrw();

int newvas(float*, int, int*, int);
int newvad(int*, int, int*, int);
void drwvas(int);
void drwvad(int, int, float*, int, int*, int);

int newshader(char*, int);
int newprogram(int, int, int);
void useprogram(int);